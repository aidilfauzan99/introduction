# Introduction

## About Myself 

<img src="picture/IMG-20190404-WA0016.jpg" width="200" length="300">
</img>
 
Name: Mohammad Aidil Fauzan bin Mohd Asli

Age : 22

From: Kemaman,Terengganu

My Current Location [:pushpin:](https://www.google.com.my/maps/place/Mutiara+Residence@Serdang/@3.0175721,101.7067758,17z/data=!3m1!4b1!4m5!3m4!1s0x31cdcaa0e9307093:0x8814dc97dfe0f593!8m2!3d3.0175181!4d101.7090033)

| Strength | Weakness |
| ------ | ------ |
| Easy to approach | shy to initiate a conversation|
| love to explore| lack of confidence |

## [Logbook](Logbook.md)

