<div align="right">Aidil </div>

# Log Book Week 3

## Agenda
Understanding the HAU research paper.

## Goals
Able to understand the fundamental of control system of an airship by referring HAU research paper

## Problem and Solution

| Poblem | Solution |
| ------ | ------ |
| Cannot attend discussion with Mr. Azizi | Get explaination from group members about the meeting |

## Impact
increase my understanding about the control system and flight dynamics of airship

## Next Step
Install and learn about mission planner and how to calibrate a quadcopter.

